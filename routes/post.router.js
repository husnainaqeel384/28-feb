const express = require('express');
const router = express.Router();

const postcontroller = require('../controller/post.controller')

// create post router
router.route("/createPost").get(postcontroller.createPost);

// Get post router
router.route("/showPost").get(postcontroller.showPost);

//  update post router
router.route("/updatePost/:updateTITLE").put(postcontroller.updatePost);
// delete post router
router.route("/deletePost/:deletetitle").delete(postcontroller.deletePost);

router.route('/register').get(postcontroller.register);

// login routers

router.route('/login').get(postcontroller.login);


module.exports = router;
