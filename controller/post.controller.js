const knex = require('../model/db');
const bcrypt = require('bcrypt')
const userdata = {

    async createPost(req, res) {

        const user = await knex('postlogin').where({ EMAIL: req.body.email }).first('*');
        if (user) {
            const psswordcheck = await bcrypt.compare(req.body.password, user.PASSWORD);
            if (!psswordcheck) {
                res.json({
                    success: false,
                    message: "You can not add create "
                })
            } else {
                const postdata = [
                    {
                        POST_ID: user.POST_ID,
                        TITLE: req.body.title,
                        DESCRIPTION: req.body.description,
                        CREATE_AT: req.body.create_at,

                    }
                ]
                const created = await knex('posts').select('POST_ID', 'TITLE', 'DESCRIPTION', 'CREATE_AT').insert(postdata);
                res.status(201).json({
                    success: true,
                    status: 201,
                })
            }

        } else {
            res.json({ success: false, message: "you can add post  please login first" });
        }
    },


    // display function

    async showPost(req, res) {
        const user = await knex('postlogin').where({ EMAIL: req.body.email }).first('*');
        if (user) {
            const psswordcheck = await bcrypt.compare(req.body.password, user.PASSWORD);
            // console.log(psswordcheck)
            if (!psswordcheck) {
                res.json({
                    success: false,
                    message: "You can not  display your post"
                })
            } else {
                const getdata = await knex('postlogin').join('posts', 'posts.POST_ID', 'postlogin.POST_ID').select('*');
                res.json({
                    success: true,
                    getdata
                })
            }
        }


    },


    //  update function


    async updatePost(req, res) {

        const user = await knex('postlogin').where({ EMAIL: req.body.email }).first('*');
        if (user) {
            const psswordcheck = await bcrypt.compare(req.body.password, user.PASSWORD);
            if (!psswordcheck) {
                res.json({
                    success: false,
                    message: "You can not  update post you updateonle your post"
                })
            } else {
                const updateTITLE = req.params.updateTITLE;
                const title = req.body.title;
                const description = req.body.description;
                const create_at = req.body.create_at;
                // check update id is present or not
                const checkid = await knex('posts').select('TITLE').where({ TITLE: updateTITLE });
                if (checkid.length != 0) {
                    const updatedata = await knex('posts').update({ TITLE: title, DESCRIPTION: description, CREATE_AT: create_at }).where({ TITLE: updateTITLE });
                    return res.json({
                        success: true,
                        message: 'Successfully updated'
                    })
                }
                res.json({
                    success: false,
                    message: "Id doesnot Exist"
                })

            }
        } else {
            res.json({ success: false, message: "you can update post  please login first" });
        }

    },


    //  delete function

    async deletePost(req, res) {

        const user = await knex('postlogin').where({ EMAIL: req.body.email }).first('*');
        if (user) {
            const psswordcheck = await bcrypt.compare(req.body.password, user.PASSWORD);
            if (!psswordcheck) {
                res.json({
                    success: false,
                    message: "You can not add create "
                })
            } else {
                const checkid = await knex('posts').select('TITLE').where({ TITLE: req.params.deletetitle });
                if (checkid.length > 0) {
                    const deletedpost = await knex('posts').delete().where({ TITLE: req.params.deletetitle });
                    return res.json({ success: true, message: "post deleted" });
                }
                res.json({ success: false, message: "Id not found" })
            }

        } else {
            res.json({ success: false, message: "you can delete post  please login first" });
        }


    },
    async register(req, res) {

        const Password = req.body.password;

        const hashpassword = await bcrypt.hash(Password, 10);

        const checkuser = await knex('postlogin').select('EMAIL').where({ EMAIL: req.body.email });

        if (checkuser.length == 0) {
            const registeruser = await knex('postlogin').insert({ EMAIL: req.body.email, PASSWORD: hashpassword, POST_ID: req.body.post_id });
            console.log(registeruser)
            res.json({
                success: true,
                status: 201,

            })
        } else {
            res.json({ success: false, message: "User already exists", })
        }
    },
    async login(req, res) {
        const user = await knex('login').where({ EMAIL: req.body.email }).first('*');
        if (user) {
            const psswordcheck = await bcrypt.compare(req.body.password, user.PASSWORD);
            const email = req.body.email;
            if (psswordcheck) {
                res.json({ success: true, message: "Login Successfully" }
                );
            } else {
                res.json({ success: false, message: "Enter email and password" });
            }
        } else {
            res.json({ success: false, message: "login false" });
        }

    }

}
module.exports = userdata
